# README #

### What is this repository for? ###

Simple script for NovAtel messages conversion to KML format.
Currently supported messages: BESTPOSA, BESTGNSSPOSA, INSPVASA, INSPVAXA, GPGGA

The script is looking for the files with .ASC or .INS extention located in the same folder. Based on single file's line, script is creating a point feature with information table derived from the message. The information written in the table is depandant on the message type.

### How do I get set up? ###

The script is working on the Python 3. In the repository you can find an example file, which can be converted with the script.

### Contribution guidelines ###

Todo:

* GPS to UTC time conversion

* Resampling option

* GPGST file processing

* Include semisphere sign (N,S,E,W)

### Who do I talk to? ###

Repo owner: Piotr Krystek